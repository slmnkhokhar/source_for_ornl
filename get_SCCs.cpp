
#include "generate_user_transaction_records.h"

using namespace std;
using namespace boost;

void get_SCCs()
{

  typedef adjacency_list < vecS, vecS, directedS > Graph; // we use an adjacency list representation for a graph
  const unsigned long N = 6336769;

  Graph G(N);

  std::ifstream myReadFile;
  myReadFile.open("ordered_user_edges.txt"); 
  unsigned long counter = 0;
  unsigned long outer_count = 0;
  string line;
  if (myReadFile.is_open()) {
  while (std::getline(myReadFile, line)) {
    std::stringstream ssline ( line );
    unsigned long values[3];
    counter = 0;
    string word;
    //    std::cout << line << std::endl;
    while (getline(ssline, word, ',')){

    	cout << "Current line : " << line << endl;
    	cout << "Current word : " << word << endl;
      if (counter > 2) break;
      values[counter] = atoi(word.c_str());
      counter++;

    }

//      std::cout<< values[0] << ", " << values[1] << ", " << values[2] << std::endl;
    outer_count++;
    if (values[1]==values[2] || (values[1] > N) || (values[2] > N)){
//    	  std::cout << "same indices found here\n";
      continue;
    }
    add_edge(values[1], values[2], G);
//      std::cout << "*********** \n\n";
    if (outer_count % 100 == 0)
      cout << outer_count << "\n";
  }
  }
  myReadFile.close();
//  t = clock() - t;
//  std::cout << "It took time : " << t;
  vector<unsigned long> c(N);

  int num = strong_components(G, make_iterator_property_map(c.begin(), get(vertex_index, G), c[0]));

  cout << "Total number of components: " << num << endl;
  vector < unsigned long >::iterator i;
  ofstream savefile;
  savefile.open ("components.txt");
  for (i = c.begin(); i != c.end(); ++i){
    //std::cout << "Vertex " << i - c.begin()
    // << " is in component " << *i << std::endl;
    savefile << i - c.begin() << ", " << *i << "\n";
  }
  savefile.close();
  //cout << "It took graph conn comp time : " << t;
}
